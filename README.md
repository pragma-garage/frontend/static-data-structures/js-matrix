# Ejemplos de uso de Matrices con JS

En este repositorio se encuentran archivos con ejemplos relacionados a la definición y uso de matrices.

## Instrucciones de uso

Cada ejemplo se encuentra descrito en un archivo `.js`, para verificarlo, basta con abrir el archivo `index.html` y desde allí navegar por cada una de las definiciones y ejemplos.

Para visualizar el resultado de los códigos que se ejecutan en cada uno de los ejemplos, basta con abrir en el navegador la opción de *Inspeccionar*, que por ejemplo en **Chrome** y con sistema operativo **Windows** se puede abrir presionando las teclas `Ctrl + Shift + I` y en sistema operativo **MACOS** presionando las teclas `Cmd + Shift + I`. Una vez abierta la herramienta los resultados se pueden ver en la pestaña *Console*.

Una vez haya abierto el `index.html` puede modificar y vericar resultados adicionales en la consola.
