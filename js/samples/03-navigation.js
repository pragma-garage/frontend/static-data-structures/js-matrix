/**
 * Recorrer los elementos de una matriz, uno a uno
 */
var runner = function () {
  // Recorrer un arreglo usando un ciclo y los índices
  let sampleMatrix = [
    [1, 0, 0, 0, 0],
    [0, 0, 2, 0, 0],
    [1, 1, 0, 0, 1],
    [2, 1, 1, 1, 0],
    [1, 2, 1, 2, 2]
  ]
  console.log(
    'Teniendo en cuenta la forma en que se accede a los elementos de una matriz, es posible recorrerla, ',
    'sea para modificarla o simplemente para mostrar uno a uno sus elementos. Si tenemos la matriz:\n',
    sampleMatrix,
    '\n\nPodemos usar dos ciclos for anidados para recorrer cada uno de sus posiciones y mostrar cada uno de sus elementos:'
  )
  for (let i = 0; i < sampleMatrix.length; i++) {
    for (let j = 0; j < sampleMatrix[i].length; j++) {
      console.log(`Posición [${i}][${j}]:`, sampleMatrix[i][j])
    }
  }
}