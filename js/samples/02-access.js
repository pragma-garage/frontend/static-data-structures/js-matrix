/**
 * Acceso a los elementos de una matriz
 */

var runner = function () {
  // Elementos individuales en una matriz
  let sampleMatrix = [
    [1, 0, 0, 0, 0],
    [0, 0, 2, 0, 0],
    [1, 1, 0, 0, 1],
    [2, 1, 1, 1, 0],
    [1, 2, 1, 2, 2]
  ]
  let firstDimensionPosition = 2
  let secondDimensionPosition = 3
  let matrixElement = sampleMatrix[firstDimensionPosition][secondDimensionPosition]
  console.log(
    'Las matrices, al ser arreglos de arreglos en JS son también de \'índice 0\', esto significa que sus elementos ',
    'se encuentran enumerados empezando con el 0',
    '\n\nPara acceder a los elementos en una matriz, basta con indicar entre corchetes la posición en cada dimensión ',
    'del elemento en cuestion. Así pues si tenemos la matriz:\n',
    sampleMatrix,
    `\n\nLa posición [${firstDimensionPosition}][${secondDimensionPosition}] corresponde al elemento con valor:`,
    matrixElement
  )

  // Modificar elementos del arreglo
  let sampleMatrixCopy = cloneMatrix(sampleMatrix)
  let firstDimensionPositionToChange = 3
  let secondDimensionPositionToChange = 1
  let newValue = 5
  sampleMatrixCopy[firstDimensionPositionToChange][secondDimensionPositionToChange] = newValue
  console.log(
    'Es posible modificar el valor de un elemento en una matriz simplemente asignandolo especificando ',
    'las posiciones adecuadas para cada dimensión.',
    '\n\nSi tenemos la siguiente matriz\n',
    sampleMatrix,
    `\n\nY se modifica el valor en la posición [${firstDimensionPositionToChange}][${secondDimensionPositionToChange}] `,
    `y se asigna como nuevo valor el ${newValue}, entonces la matriz quedaría de la siguiente manera:\n`,
    sampleMatrixCopy
  )
}

/** @param {Array} input */
function cloneMatrix(input) {
  let output = []
  input.forEach((value, index) => {
    output[index] = Array.isArray(value) ? cloneMatrix(value) : value
  })
  return output
}