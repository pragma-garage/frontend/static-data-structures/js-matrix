/**
 * Definición de una matriz en JS
 */
var runner = function () {
  // Definir matrices con sus elementos
  let sampleMatrix = [
    [1, 0, 0, 0, 0],
    [0, 0, 2, 0, 0],
    [1, 1, 0, 0, 1],
    [2, 1, 1, 1, 0],
    [1, 2, 1, 2, 2]
  ]
  let multiDimesionArray = [
    sampleMatrix,
    sampleMatrix
  ]
  console.log(
    'Las matrices se pueden ver como arreglos de más de una dimensión o también llamados \'Arreglos de arreglos\'',
    '\n\nPor ejemplo una matriz de elementos numéricos de dos dimensiones podría ser la siguiente:\n',
    sampleMatrix,
    '\n\nPor tanto, al ser arreglos se pueden dar casos con matrices con más de dos dimensiones como la siguiente:\n',
    multiDimesionArray
  )
}